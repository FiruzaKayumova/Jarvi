var gulp    = require('gulp'),
    sass    = require('gulp-sass'),
    concat  = require('gulp-concat'),
    uglify  = require('gulp-uglifyjs'),
    pug     = require('gulp-pug'),
    del     = require('del');


    gulp.task('sass', function() {
    	return gulp.src("app/scss/*.scss")
    	.pipe(sass())
    	.pipe(gulp.dest("dist/css"))
    })

    gulp.task('scripts', function() {
    	return gulp.src([
    		"node_modules/jquery/dist/jquery.min.js",
    		"node_modules/bootstrap/dist/js/bootstrap.min.js"
    		])
    	.pipe(concat("libs.min.js"))
    	.pipe(uglify())
    	.pipe(gulp.dest("dist/js"))
    })

    gulp.task('pug', function() {
    	return gulp.src('app/pug/index.pug')
    	.pipe(pug({pretty: true}))
    	.pipe(gulp.dest("dist"))
    })
    
    gulp.task('watch', ['scripts'], function() {
        gulp.watch("app/scss/**/*.scss", ['sass'])
        gulp.watch('app/pug/index.pug', ['pug'])
    })

    gulp.task('build', ['sass', 'scripts', 'pug'], function() {
        var buildJs = gulp.src([
            'app/js/**/*'
            ])
        .pipe(gulp.dest('dist/js'));

        var buildCss = gulp.src(
            'node_modules/bootstrap/dist/css/bootstrap.min.css'
            )
        .pipe(gulp.dest('dist/css'))

        var buildFont = gulp.src(
            'app/fonts/*'
            )
        .pipe(gulp.dest('dist/fonts'))
    })  